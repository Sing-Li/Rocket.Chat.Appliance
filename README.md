![Rocket.Chat logo](https://rocket.chat/images/logo/logo-dark.svg?v2)

# Rocket.Chat Appliance

### Background

Our entire core team and active contributors grew up and are completely immersed in the world of cloud computing and ever-present high speed Internet connections.   

Yet day after day, community members enter our chat room and tell us that they live their lives behind a corporate firewall; or that they want to run  Rocket.Chat on their Raspberry Pi, Beaglebone Black, or octa-core ODroid-XU4.

Thanks for your feedback.  It opened our eyes and we hear you loud and clear!

### Mission

Rocket.Chat Appliance, the project, is chartered to deliver an easy to configure and deploy Rocket.Chat server in a box.  

Everything in Rocket.Chat Appliance will be 100% FOSS, and developed under the liberal MIT license by our core team and the extended global team of expert contributors. 

From two users to two hundred thousand users. The Rocket.Chat appliance will scale from the needs of the individual maker/hobbiest, right up to those of the largest corporation.